#!/usr/bin/env python

""" Global settings file """

import os

CSRF_ENABLED = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

DEBUG = os.getenv('FLASK_DEBUG', default='False')
TESTING = os.getenv('FLASK_TESTING', default='False')
ENV = os.getenv('FLASK_ENV', default='production')

FLASK_HOST = os.getenv('FLASK_HOST', default='127.0.0.1')
FLASK_PORT = os.getenv('FLASK_PORT', default='5000')
FLASK_SECRET = os.getenv('FLASK_SECRET')

DATABASE_TYPE = os.getenv('DATABASE_TYPE')
DATABASE_HOST = os.getenv('DATABASE_HOST')
DATABASE_PORT = os.getenv('DATABASE_PORT')
DATABASE_NAME = os.getenv('DATABASE_NAME')
DATABASE_USER = os.getenv('DATABASE_USER')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD')

SQLALCHEMY_DATABASE_URI = '{}://{}:{}@{}:{}/{}'.format(DATABASE_TYPE,
                                                       DATABASE_USER,
                                                       DATABASE_PASSWORD,
                                                       DATABASE_HOST,
                                                       DATABASE_PORT,
                                                       DATABASE_NAME)
