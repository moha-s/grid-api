#!/usr/bin/env python

""" Models """

from . import DB

SECOND_PLAYER = "o"


class Grid(DB.Model):

    """This class represents a tic tac toe grid."""

    __tablename__ = 'grids'

    id = DB.Column(DB.Integer, primary_key=True)
    date_created = DB.Column(DB.DateTime, default=DB.func.current_timestamp())
    date_modified = DB.Column(
        DB.DateTime, default=DB.func.current_timestamp(),
        onupdate=DB.func.current_timestamp())

    # Grid representation
    square_0 = DB.Column(DB.String(1), default=None)
    square_1 = DB.Column(DB.String(1), default=None)
    square_2 = DB.Column(DB.String(1), default=None)
    square_3 = DB.Column(DB.String(1), default=None)
    square_4 = DB.Column(DB.String(1), default=None)
    square_5 = DB.Column(DB.String(1), default=None)
    square_6 = DB.Column(DB.String(1), default=None)
    square_7 = DB.Column(DB.String(1), default=None)
    square_8 = DB.Column(DB.String(1), default=None)

    last_player = DB.Column(DB.String(1), default=SECOND_PLAYER)

    winner = DB.Column(DB.String(1), default=None)

    def save(self):
        """
        save grid in db
        """
        DB.session.add(self)
        DB.session.commit()

    @staticmethod
    def get_all():
        """
        get all grids from db
        """
        return Grid.query.all()

    def delete(self):
        """
        delete a grid from db
        """
        DB.session.delete(self)
        DB.session.commit()

    def __repr__(self):
        """
        return a representation of the grid instace
        """
        return "<Grid: {}>".format(self.id)

    def get_data(self):
        """
        return all data from grid as a dict
        """
        return {'id': self.id,
                'date_created': self.date_created,
                'date_modified': self.date_modified,
                'content': [self.square_0, self.square_1, self.square_2,
                            self.square_3, self.square_4, self.square_5,
                            self.square_6, self.square_7, self.square_8],
                'last_player': self.last_player,
                'winner': self.winner}
