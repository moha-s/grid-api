#!/usr/bin/env sh

# This script permit to prepare the startup of grid-api container

# Run database migration
pipenv run python manage.py db upgrade

# Run CMD passed to container
exec "$@"
